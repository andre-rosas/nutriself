import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { api } from "../../services/api";
import jwt_decode from "jwt-decode";
import { useEditProfileContext } from "../../providers/EditProfile";
import toast from "react-hot-toast";
interface IMeal {
  name: string;
  foodsId: number[];
  id?: number;
}
interface IUserDietData {
  dietPlan: IMeal[];
  addFoodToDiet: (foodId: number, idx: number) => void;
  removeFoodFromDiet: (foodId: number, idx: number, id: number) => void;
  userTMB: number;
  macros: IMacros;
  setMealId: (mealId: number | undefined) => void;
  newFoodsIds: number[];
  getDiet: () => void;
  basalMetabolicRate: () => void;
}
interface IMacros {
  protein: number;
  fat: number;
  carbs: number;
}
interface IUserDietProviderProps {
  children: ReactNode;
}
interface IObj {
  foodsId: number[];
}
const UserDietContext = createContext<IUserDietData>({} as IUserDietData);
export const UserDietProvider = ({ children }: IUserDietProviderProps) => {
  const token = localStorage.getItem("accesstoken") || "";
  const [auth] = useState<string>(token && jwt_decode(JSON.stringify(token)));
  const [newFoodsIds, setNewFoodsIds] = useState<number[]>([]);
  const [mealId, setMealId] = useState<number | undefined>(0);
  const [userTMB, setUserTMB] = useState<number>(0);
  const [macros, setMacros] = useState<IMacros>({} as IMacros);
  const bearer = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  const { dadosDoUsuario } = useEditProfileContext();
  const userId = localStorage.getItem("dadosDoUsuario");
  const initialState = [
    { name: "Café da Manhã", foodsId: [], userId: userId },
    { name: "Almoço", foodsId: [], userId: userId },
    { name: "Lanche", foodsId: [], userId: userId },
    { name: "Jantar", foodsId: [], userId: userId },
  ];
  const [dietPlan, setDietPlan] = useState(initialState);
  useEffect(() => {
    getDiet();
  }, [newFoodsIds]);
  const getDiet = () => {
    api.get(`/feed?userId=${userId}`, bearer).then((res) => {
      if (res.data.length > 0) {
        setDietPlan(res.data);
      } else {
        api
          .post(`/feed`, initialState[0], bearer)
          .then(() => api.post(`/feed`, initialState[1], bearer))
          .then(() => api.post(`/feed`, initialState[2], bearer))
          .then(() => api.post(`/feed`, initialState[3], bearer));
      }
    });
  };
  useEffect(() => {
    updateMeal(mealId);
  }, [newFoodsIds]);
  const updateMeal = (mealId: number | undefined) => {
    api.patch(`/feed/${mealId}`, { foodsId: newFoodsIds }, bearer);
  };
  const addFoodToDiet = async (foodId: number, mealId: number) => {
    const res = await api.get(`/feed/${mealId}`, bearer);
    setNewFoodsIds([...res.data.foodsId, foodId]);
    toast.success("Comida adicionada à refeição!");
  };
  const removeFoodFromDiet = (foodId: number, mealId: number, idx: number) => {
    api
      .get(`/feed/${mealId}`, bearer)
      .then((res) =>
        setNewFoodsIds(
          [...res.data.foodsId].filter((elem: number, id: number) => idx !== id)
        )
      );
  };
  const basalMetabolicMale =
    5 +
    10 * dadosDoUsuario.weight +
    6.25 * dadosDoUsuario.height -
    5 * dadosDoUsuario.age;
  const basalMetabolicFemale =
    10 * dadosDoUsuario.weight +
    6.25 * dadosDoUsuario.height -
    5 * dadosDoUsuario.age -
    161;
  const basalMetabolicRate = () => {
    if (dadosDoUsuario.activity === "Sedentarismo") {
      if (dadosDoUsuario.gender === "male") {
        setUserTMB(
          Math.ceil(
            basalMetabolicMale *
              1.2 *
              (dadosDoUsuario.objective === "Perder Peso" ? 0.8 : 1.1)
          )
        );
      } else {
        setUserTMB(
          Math.ceil(
            basalMetabolicFemale *
              1.1 *
              (dadosDoUsuario.objective === "Perder Peso" ? 0.8 : 1.1)
          )
        );
      }
    } else if (dadosDoUsuario.activity === "Moderado") {
      if (dadosDoUsuario.gender === "male") {
        setUserTMB(
          Math.ceil(
            basalMetabolicMale *
              1.55 *
              (dadosDoUsuario.objective === "Perder Peso" ? 0.8 : 1.1)
          )
        );
      } else {
        setUserTMB(
          Math.ceil(
            basalMetabolicFemale *
              1.55 *
              (dadosDoUsuario.objective === "Perder Peso" ? 0.8 : 1.1)
          )
        );
      }
    } else {
      if (dadosDoUsuario.gender === "male") {
        setUserTMB(
          Math.ceil(
            basalMetabolicMale *
              1.725 *
              (dadosDoUsuario.objective === "Perder Peso" ? 0.8 : 1.1)
          )
        );
      } else {
        setUserTMB(
          Math.ceil(
            basalMetabolicFemale *
              1.725 *
              (dadosDoUsuario.objective === "Perder Peso" ? 0.8 : 1.1)
          )
        );
      }
    }
  };
  const macrosCalc = () => {
    if (userTMB !== 0) {
      setMacros({
        protein: Math.ceil((userTMB * 0.2) / 4),
        fat: Math.ceil((userTMB * 0.3) / 9),
        carbs: Math.ceil((userTMB * 0.5) / 4),
      });
    }
  };
  useEffect(() => {
    macrosCalc();
    basalMetabolicRate();
  }, [dadosDoUsuario, userTMB]);
  return (
    <UserDietContext.Provider
      value={{
        dietPlan,
        addFoodToDiet,
        removeFoodFromDiet,
        userTMB,
        macros,
        setMealId,
        newFoodsIds,
        getDiet,
        basalMetabolicRate,
      }}
    >
      {children}
    </UserDietContext.Provider>
  );
};
export const useUserDietContext = () => useContext(UserDietContext);
