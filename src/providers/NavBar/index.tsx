import { createContext, ReactNode, useContext, useState } from "react";

interface INavBarProviderProps {
  children: ReactNode;
}

interface INavBarData {
  selected: string;
  changeSelected: (selected: string) => void;
}

const NavBarContext = createContext<INavBarData>({} as INavBarData);

export const NavBarProvider = ({ children }: INavBarProviderProps) => {
  const [selected, setSelected] = useState<string>("user");

  const changeSelected = (selected: string) => {
    setSelected(selected);
  };

  return (
    <NavBarContext.Provider
      value={{
        selected,
        changeSelected,
      }}
    >
      {children}
    </NavBarContext.Provider>
  );
};

export const useNavBar = () => useContext(NavBarContext);
