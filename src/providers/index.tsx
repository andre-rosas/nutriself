import { ReactNode } from "react";
import { CarouselProvider } from "./Carousel";
import { AuthProvider } from "./Auth";
import { FoodsProvider } from "./Foods";
import { EditProfileProvider } from "./EditProfile";
import { UserDietProvider } from "./UserDiet";
import { NavBarProvider } from "./NavBar";

interface IProviderProps {
  children: ReactNode;
}

export const Providers = ({ children }: IProviderProps) => {
  return (
    <CarouselProvider>
      <FoodsProvider>
        <EditProfileProvider>
          <UserDietProvider>
            <NavBarProvider>
              <AuthProvider>{children}</AuthProvider>
            </NavBarProvider>
          </UserDietProvider>
        </EditProfileProvider>
      </FoodsProvider>
    </CarouselProvider>
  );
};
