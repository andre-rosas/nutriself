import { createContext, ReactNode, useContext, useState } from "react";

interface ICarouselProviderProps {
  children: ReactNode;
}

interface ICarouselData {
  currentImg: number;
  changeImgIdx: (idx: number) => void;
}

const CarouselContext = createContext<ICarouselData>({} as ICarouselData);

export const CarouselProvider = ({ children }: ICarouselProviderProps) => {
  const [currentImg, setCurrentImg] = useState<number>(0);

  const changeImgIdx = (idx: number) => {
    setCurrentImg(idx);
  };

  return (
    <CarouselContext.Provider value={{ currentImg, changeImgIdx }}>
      {children}
    </CarouselContext.Provider>
  );
};

export const useCarouselContext = () => useContext(CarouselContext);
