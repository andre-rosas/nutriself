// import { ReactNode } from "react";

export interface BulletComponent {
  bulletIdx: number;
  currentIdx: number;
}

export interface IUserData {
  username: string;
  email: string;
  password: string;
}

export interface ILoginData {
  email: string;
  password: string;
}

export interface NavBar {
  selected: string;
  name: string;
}
