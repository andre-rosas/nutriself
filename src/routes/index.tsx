import { Route, Switch } from "react-router-dom";
import { AddFoods } from "../pages/AddFoods";
import { DietPlan } from "../pages/DietPlan";
import { Editprofile } from "../pages/EditProfile";
import { Login } from "../pages/Login";
import { PreLogin } from "../pages/PreLogin";
import { Profile } from "../pages/Profile";
import { Register } from "../pages/Register";
import { Terms } from "../pages/Terms";
import { About } from "../pages/About";
import { Search } from "../pages/Search";
import { NotFound } from "../pages/NotFound";
import { PrivateRoute, PublicRoute } from "./route";

export const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={PreLogin} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/register" component={Register} />
      <Route exact path="/register/:formSteps" component={Register} />
      <Route exact path="/terms" component={Terms} />
      <Route exact path="/about" component={About} />
      <PrivateRoute exact path="/profile" component={Profile} />
      <PrivateRoute exact path="/editprofile" component={Editprofile} />
      <PrivateRoute exact path="/dietPlan/:pageType" component={DietPlan} />
      <PrivateRoute exact path="/search" component={Search} />
      <PrivateRoute exact path="/addFood/:mealId" component={AddFoods} />
      <Route component={NotFound} />
    </Switch>
  );
};
