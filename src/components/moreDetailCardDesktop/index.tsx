import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

import { Container } from "./style";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 500,
      minHeight: 15,
      border: "0px 0px 1px 0px solid black",
      marginBottom: "15px",
      margin: "0 auto",
      padding: 0,
    },
  })
);

interface IMoreDetail {
  fiber: number;
  saturated: number;
  monounsaturated: number;
  polyunsaturated: number;
  sodium: number;
  potassium: number;
  cholesterol: number;
}

const MoreDetailCardDesktop = ({
  fiber,
  saturated,
  monounsaturated,
  polyunsaturated,
  sodium,
  potassium,
  cholesterol,
}: IMoreDetail) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Container>
          <h3>Carboidratos</h3>
          <div className="nutrients">
            <span>Fibras</span>
            <span className="grams">{fiber}g</span>
          </div>
        </Container>
        <Container>
          <h3>Gorduras</h3>
          <div className="nutrients">
            <span>Gorduras Saturadas</span>
            <span className="grams">{saturated}g</span>
          </div>
          <div className="nutrients">
            <span>Gorduras Poliinsaturadas</span>
            <span className="grams">{polyunsaturated}g</span>
          </div>
          <div className="nutrients">
            <span>Gorduras Monoinsaturadas</span>
            <span className="grams">{monounsaturated}g</span>
          </div>
        </Container>

        <Container>
          <h3>Outros</h3>
          <div className="nutrients">
            <span>Sódio</span>
            <span className="grams">{sodium}mg</span>
          </div>
          <div className="nutrients">
            <span>Colesterol</span>
            <span className="grams">{cholesterol}mg</span>
          </div>
          <div className="nutrients">
            <span>Potássio</span>
            <span className="grams">{potassium}mg</span>
          </div>
        </Container>
      </CardContent>
    </Card>
  );
};

export default MoreDetailCardDesktop;
