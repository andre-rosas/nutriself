import { Container, InputContainer } from "./styles";

interface ItextField extends React.InputHTMLAttributes<HTMLInputElement> {
  register: any;
  label: string;
  name: string;
  placeholder: string;
  error: any;
  icon: React.ComponentType;
}

const TextField = ({
  label,
  icon: Icon,
  register,
  error,
  name,
  ...rest
}: ItextField) => {
  return (
    <Container>
      <div className="label--input">
        <div>{label}</div>
      </div>
      <InputContainer>
        <div className="iconContainer">
          <Icon />
        </div>
        <input {...register(name)} {...rest} />
      </InputContainer>
      {!!error && <span>{error}</span>}
    </Container>
  );
};

export default TextField;
