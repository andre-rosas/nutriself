import styled from "styled-components";

interface progressBar {
  progress: number;
  foodGrans: number;
  totalGrans: number;
}

export const ProgressBar = styled.div<progressBar>`
  margin-bottom: 18px;
  width: 65px;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;

  .border {
    border: 1px solid black;
    width: 65px;
    height: fit-content;
  }
  .graph {
    background-color: ${(props) =>
      (props.foodGrans / props.totalGrans) * 100 <= 100
        ? "var(--color-primary)"
        : "var(--color-terciary)"};
    height: 8px;
    width: ${(props) => `${props.progress}%`};
  }
  .macros {
    margin-top: 3px;
    margin-bottom: 18px;
    font-size: 13px;
  }

  .graphDetail {
    margin-bottom: 3px;
    font-size: 13px;
  }
`;
