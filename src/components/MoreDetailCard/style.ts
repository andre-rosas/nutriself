import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 15px;

  h3 {
    font-size: 18px;
  }

  .nutrients {
    display: flex;
    justify-content: start;
    border-bottom: 1px solid black;
    margin-bottom: 7px;
  }

  .grams {
    margin-left: auto;
  }
`;
