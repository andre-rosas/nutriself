import styled from "styled-components";

export const ButtonStyle = styled.button`
  width: 214px;
  height: 54px;
  color: var(--color-base-default);

  background: var(--color-primary);
  border-radius: 48px;
  border: 2px solid transparent;

  font-size: 24px;
  font-family: "Signika", sans-serif;

  transition: all 0.5s;
  margin: 16px 8px;
  &:hover {
    background: var(--color);
    color: var(--color-primary);
    border: 2px solid var(--color-primary);
  }
  &:active {
    background: var(--color-terciary);
    color: var(--color-terciary-two);
    border: 2px solid var(--color-terciary-two);
  }
`;
