import styled from "styled-components";
import { NavBar } from "../../types";

export const NavbarContainer = styled.div`
  display: flex;
  width: 100%;
  height: 50px;
  background-color: #91c787;
  justify-content: space-around;
  align-items: center;
  position: sticky;
  bottom: 0;
  box-shadow: 0px 1px 4px -2px black inset;

  @media (min-width: 768px) {
    display: none;
  }
`;

export const IconNavBar = styled.a<NavBar>`
  color: #f2f2f2;
  padding: 7px 8px 5px;
  background-color: ${(props) =>
    props.selected === props.name ? "#a52451" : "none"};
  border-radius: 16px;

  svg {
    filter: drop-shadow(0px 3px 2px rgba(0, 0, 0, 0.7));
  }
`;
