import { FaHome, FaSearch, FaUser, FaClipboardList } from "react-icons/fa";
import { GrMail } from "react-icons/gr";
import { IconNavBar, NavbarContainer } from "./style";
import { useHistory } from "react-router-dom";
import { useNavBar } from "../../providers/NavBar";
import toast from "react-hot-toast";

export const Navbar = () => {
  const { selected, changeSelected } = useNavBar();
  const history = useHistory();

  return (
    <NavbarContainer>
      <IconNavBar
        name="home"
        selected={selected}
        onClick={() => {
          changeSelected("home");
          history.push("/dietPlan/choose");
        }}
      >
        <FaHome />
      </IconNavBar>
      <IconNavBar
        name="search"
        selected={selected}
        onClick={() => {
          changeSelected("search");
          history.push("/search");
        }}
      >
        <FaSearch />
      </IconNavBar>
      <IconNavBar
        name="user"
        selected={selected}
        onClick={() => {
          changeSelected("user");
          history.push("/profile");
        }}
      >
        <FaUser />
      </IconNavBar>
      <IconNavBar
        name="clipboard"
        selected={selected}
        onClick={() => {
          changeSelected("clipboard");
          history.push("/dietPlan/choose");
        }}
      >
        <FaClipboardList />
      </IconNavBar>
      <IconNavBar
        name="mail"
        selected={selected}
        onClick={() => {
          changeSelected("mail");
          toast.error("Em construção!")
        }}
      >
        <GrMail />
      </IconNavBar>
    </NavbarContainer>
  );
};
