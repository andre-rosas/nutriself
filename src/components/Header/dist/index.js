"use strict";
exports.__esModule = true;
exports.Header = void 0;
var nutriselfUserLogo_png_1 = require("../../assets/nutriselfUserLogo.png");
var cg_1 = require("react-icons/cg");
var EditProfile_1 = require("../../providers/EditProfile");
var react_1 = require("react");
var styles_1 = require("./styles");
var react_router_dom_1 = require("react-router-dom");
exports.Header = function () {
    var _a = EditProfile_1.useEditProfileContext(), PegarDadosDoUsuario = _a.PegarDadosDoUsuario, dadosDoUsuario = _a.dadosDoUsuario;
    react_1.useEffect(function () {
        PegarDadosDoUsuario();
    }, []);
    var history = react_router_dom_1.useHistory();
    var _b = react_1.useState(false), menuDrop = _b[0], setMenuDrop = _b[1];
    var toSend = function (path) {
        return history.push(path);
    };
    var OpenMenu = function () {
        setMenuDrop(true);
    };
    var CloseMenu = function () {
        setMenuDrop(false);
    };
    var handleLogout = function () {
        localStorage.clear();
        history.push("/");
    };
    return (React.createElement(styles_1.HeaderContainer, null,
        React.createElement("div", { className: "imgContainer" },
            React.createElement("img", { src: nutriselfUserLogo_png_1["default"], alt: "logo" }),
            React.createElement("span", null,
                "Ol\u00E1, ",
                dadosDoUsuario && dadosDoUsuario.name,
                "!")),
        menuDrop ? (React.createElement(cg_1.CgMenuGridR, { size: 30, color: "91c787", className: "menu--close", onClick: CloseMenu })) : (React.createElement(cg_1.CgMenuGridR, { size: 30, color: "222222", className: "menu--open", onClick: OpenMenu })),
        menuDrop ? (React.createElement(styles_1.Menuhamburguer, null,
            React.createElement("li", null,
                React.createElement(styles_1.MenuButton, { onClick: function () { return toSend("/Community"); } }, "Comunidade")),
            React.createElement("li", null,
                React.createElement(styles_1.MenuButton, { onClick: function () { return toSend("/Habits"); } }, "H\u00E1bitos")),
            React.createElement("li", null,
                React.createElement(styles_1.MenuButton, { onClick: function () { return toSend("/Groups"); } }, "Grupos")),
            React.createElement("li", null,
                React.createElement(styles_1.MenuButton, { onClick: handleLogout }, "Sair")),
            React.createElement("li", null,
                React.createElement(styles_1.MenuButton, { onClick: function () { return toSend("/About"); } }, "Sobre")))) : (React.createElement(React.Fragment, null)),
        React.createElement(styles_1.ButtonFlexContainer, null,
            React.createElement(styles_1.MenuButton, { onClick: function () { return toSend("/Community"); } }, "Comunidade"),
            React.createElement(styles_1.MenuButton, { onClick: function () { return toSend("/Habits"); } }, "H\u00E1bitos"),
            React.createElement(styles_1.MenuButton, { onClick: function () { return toSend("/Groups"); } }, "Grupos"),
            React.createElement(styles_1.MenuButton, { onClick: handleLogout }, "Sair"),
            React.createElement(styles_1.MenuButton, { onClick: function () { return toSend("/About"); } }, "Sobre"))));
};
