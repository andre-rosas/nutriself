import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 100vw;
  height: 50px;
  align-items: center;

  h5 {
    margin: auto;
    padding-top: 8px;
    font-size: 16px;
    @media (min-width: 768px){
      padding-top:8px;
      font-size: 24px;
    }
  }
`;
export const Voltar = styled.div`
  position: absolute;
  margin-left: 16px;

  @media (min-width: 768px){
    display: none;
  }
  span {
    font-size: 36px;
    @media (min-width: 768px){
    }
  }
  cursor: pointer;
`;

