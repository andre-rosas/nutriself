import { useCarouselContext } from "../../providers/Carousel";
import { Bullet, BulletsContainer } from "./style";

export const CarouselIndex = () => {
  const { currentImg } = useCarouselContext();

  return (
    <BulletsContainer>
      <Bullet bulletIdx={0} currentIdx={currentImg} />
      <Bullet bulletIdx={1} currentIdx={currentImg} />
      <Bullet bulletIdx={2} currentIdx={currentImg} />
    </BulletsContainer>
  );
};
