import styled from "styled-components";
import { BulletComponent } from "../../types";

export const BulletsContainer = styled.div`
  display: flex;
  width: 50px;
  justify-content: space-between;
  margin: 15px auto;
`;

export const Bullet = styled.div<BulletComponent>`
  width: ${(props) => (props.bulletIdx === props.currentIdx ? "20px" : "12px")};
  height: 10px;
  border-radius: 5px;
  background-color: ${(props) =>
    props.bulletIdx === props.currentIdx ? "#A52451" : "#E69CA3"};
  transition: all 0.2s;
`;
