import axios from "axios";

export const api = axios.create({
  baseURL: "https://nutriself.herokuapp.com",
});

export const apiFoods = axios.create({
  baseURL: "https://nutriself.herokuapp.com/foods",
});
