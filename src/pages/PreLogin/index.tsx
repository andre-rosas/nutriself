import { Link } from "react-router-dom";
import { CarouselComponent } from "../../components/Carousel";
import logoSecondarySelf from "../../assets/logo-secondarySelf.png";
import logoAlternativeGreen from "../../assets/logo-alternativegreen.png";
import { CarouselIndex } from "../../components/CarouselIndex";
import {
  CenterContainer,
  ContainerPreLogin,
  LeftContainerDesktop,
} from "./styles";
import { RightContainerDesktop } from "./styles";
import Button from "../../components/Button";
import { useHistory } from "react-router";
export const PreLogin = () => {
  const history = useHistory();
  return (
    <ContainerPreLogin>
      <LeftContainerDesktop>
        <img
          className="Logo--Desktop__prelogin"
          src={logoAlternativeGreen}
          alt="Logo"
        />
      </LeftContainerDesktop>
      <CenterContainer>
        <img
          className="Logo--Desktop__prelogin"
          src={logoSecondarySelf}
          alt="Logo"
        />
        <CarouselComponent />
        <CarouselIndex />
        <Button
          className="start--prelogin"
          type="submit"
          onClick={() => history.push("/register")}
        >
          Começar agora
        </Button>
        <p className="desc--Desktop__prelogin">
          Já possui uma conta? Entre <Link to="/login">aqui</Link>.
        </p>
        <div>
          <Button
            className="About_us"
            type="submit"
            onClick={() => history.push("/about")}
          >
            Sobre nós
          </Button>
        </div>
      </CenterContainer>
      <RightContainerDesktop>
        <img
          className="Logo--Desktop__prelogin"
          src={logoAlternativeGreen}
          alt="Logo"
        />
        <Button
          className="start-prelogin"
          type="submit"
          onClick={() => history.push("/register")}
        >
          Começar agora
        </Button>
        <p>
          Já possui uma conta? Entre <Link to="/login">aqui</Link>.
        </p>
      </RightContainerDesktop>
    </ContainerPreLogin>
  );
};
