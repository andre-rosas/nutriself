import styled from "styled-components";

export const BackAndTitle = styled.div`
  text-align: center;

  margin: 10px 0;

  div {
    width: fit-content;
    float: left;
  }

  p {
    margin: 0 auto;
  }
`;

export const SearchContainer = styled.div`
  width: 90%;
  margin: 15px auto;
  display: flex;
  justify-content: space-between;
  background-color: #f4f4f4;
  padding: 10px 25px;
  border-radius: 15px;
  max-width: 450px;

  input {
    width: 100%;
    border: none;
    background-color: transparent;
  }

  div {
    color: #666666;
    cursor: pointer;
  }
`;

export const FoodCard = styled.div`
  background-color: #eff7ee;
  width: 90%;
  padding: 10px;
  border-radius: 15px;
  margin: 5px auto;
  box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.3);

  h2 {
    font-size: 25px;
  }

  .calories {
    color: #6cb663;
  }
`;

export const Details = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const AddButton = styled.button`
  width: 21px;
  height: fit-content;
  border: none;
  border-radius: 12px;
  background-color: var(--color-terciary);
  color: white;
  font-size: 17px;

  &:active {
    transform: translateY(2px);
    box-shadow: 0px -1px 2px rgba(255, 255, 255, 0.8);
  }
`;

export const ParentContainer = styled.div`
  ul {
    max-width: 500px;
    margin: 0 auto;
  }
`;
