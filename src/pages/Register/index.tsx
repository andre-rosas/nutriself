import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Female from "../../assets/femaleOption.png";
import Male from "../../assets/maleOption.png";
import Other from "../../assets/otherOption.png";
import GainMuscle from "../../assets/gainMuscle.svg";
import LoseWeight from "../../assets/loseWeight.svg";
import GainWeight from "../../assets/gainWeight.svg";
import Sedentarism from "../../assets/sedentarism.png";
import Moderate from "../../assets/moderateExercise.png";
import Intense from "../../assets/intenseExercise.svg";
import { api } from "../../services/api";
import { useHistory } from "react-router";
import NutriselfLogo from "../../assets/logo-default.png";
import NutriselfLogo2 from "../../assets/logo-secondarySelf.png";
import toast from "react-hot-toast";
import { Container, ContainerRegister, Button } from "./styles";
import { AiFillLock, AiFillMail } from "react-icons/ai";
import TextField from "../../components/TextField";
import { Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

interface IFormInputs {
  name: string;
  email: string;
  password: string;
  birthday: string;
  weight: number;
  height: number;
  gender: string;
  objective: string;
  activity: string;
}

interface ContainerProps {
  backgroundColor: string;
  step: number;
  gender: string;
  objective: string;
  activity: string;
}

export const Register: React.FC<ContainerProps> = ({ backgroundColor }) => {
  const [step, setStep] = useState<number>(1);
  const [weightImc, setWeightImc] = useState<any>(65);
  const [heightImc, setHeightImc] = useState<any>(150);
  const history = useHistory();
  const [gender, setGender] = useState<string>("");
  const [objective, setObjective] = useState<string>("");
  const [activity, setActivity] = useState<string>("");
  const [birthday, setBirthday] = useState<string>("2000-12-12");

  const formSchema = yup.object().shape({
    name: yup
      .string()
      .required("Nome de usuário obrigatório")
      .max(13, "Máximo 13 caracteres"),
    email: yup
      .string()
      .required("E-mail obrigatório")
      .matches(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "E-mail inválido"
      ),
    password: yup
      .string()
      .required("Senha obrigatória")
      .min(6, "Mínimo 6 caracteres"),
    birthday: yup.string().required("Idade obrigatória"),
    weight: yup.number().required("Peso obrigatório"),
    height: yup.number().required("Altura obrigatória"),
    gender: yup.string().required("Gênero obrigatório"),
    objective: yup.string().required("Objetivo obrigatório"),
    activity: yup.string().required("Objetivo obrigatório"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm<IFormInputs>({
    resolver: yupResolver(formSchema),
  });

  const onSubmit = ({
    name,
    email,
    password,
    birthday,
    weight,
    height,
    gender,
    objective,
    activity,
  }: IFormInputs) => {
    console.log(weight, height);
    api
      .post("/users", {
        name,
        email,
        password,
        birthday,
        weight,
        height,
        gender,
        objective,
        activity,
      })
      .then((_) => history.push("/login"))
      .catch((err) => console.log(err));
  };

  const handleNextStep = () => {
    const values = getValues(["birthday", "gender", "objective", "activity"]);
    switch (step) {
      case 1:
        values[0] === null
          ? toast.error("Selecione uma opção")
          : setStep(step + 1);
        break;
      case 2:
        values[1] === null || values[2] === null
          ? toast.error("Selecione uma opção de Gênero e Objetivo")
          : setStep(step + 1);
        break;
      case 3:
        values[3] === null
          ? toast.error("Selecione uma opção de atividade")
          : setStep(step + 1);
        break;
      default:
        break;
    }
  };

  return (
    <Container
      backgroundColor={backgroundColor}
      step={step}
      gender={gender}
      objective={objective}
      activity={activity}
    >
      <div className="header">
        <figure>
          {step !== 4 ? (
            <img src={NutriselfLogo} alt="logo"></img>
          ) : (
            <img src={NutriselfLogo2} alt="logo"></img>
          )}
        </figure>
        <>
          {step !== 4 ? (
            <p className="title">
              Conte mais sobre <span className="you">você</span>!
            </p>
          ) : (
            <h3>Crie aqui sua conta!</h3>
          )}
        </>
      </div>
      <form onSubmit={handleSubmit(onSubmit)}>
        {step === 1 ? (
          <div className="step1">
            <div className="selectContainer">
              <h3>Data de Nascimento:</h3>
              <input
                  type="text"
                  value={birthday}
                  className="selectP"
                  {...register("birthday")}
                  onChange={(e) => setBirthday(e.target.value)}
                />
            </div>
            <div className="selectContainer">
              <h3>Teu peso (kg):</h3>
              <select
                {...register("weight")}
                defaultValue={weightImc}
                onChange={(e) => setWeightImc(e.target.value)}
              >
                {[...Array(280)].map((_, i) => (
                  <option key={i}>{i + 30}</option>
                ))}
              </select>
            </div>
            <div className="selectContainer">
              <h3>Tua altura (cm):</h3>
              <select
                {...register("height")}
                defaultValue={heightImc}
                onChange={(e) => setHeightImc(e.target.value)}
              >
                {[...Array(184)].map((_, i) => (
                  <option key={i}>{i + 60}</option>
                ))}
              </select>
            </div>
            <div className="imc">
              <p>IMC (kg/m²) </p>
              <div className="calcImc">
                <span>
                  {(
                    weightImc /
                    (((heightImc / 100) * heightImc) / 100)
                  ).toFixed(2)}
                </span>
              </div>
            </div>
          </div>
        ) : null}
        {step === 2 ? (
          <div className="step2">
            <h3>Teu gênero</h3>
            <div className="genderContainer">
              <label className="man">
                <input
                  type="radio"
                  value="male"
                  className="radioInput"
                  onClick={() => setGender("male")}
                  {...register("gender")}
                />
                <img src={Male} alt="Homem" />
                <span>Homem</span>
              </label>
              <label className="woman">
                <input
                  type="radio"
                  value="female"
                  className="radioInput"
                  onClick={() => setGender("female")}
                  {...register("gender")}
                />
                <img src={Female} alt="Mulher" />
                <span>Mulher</span>
              </label>
              <label className="other">
                <input
                  type="radio"
                  value="other"
                  className="radioInput"
                  onClick={() => setGender("other")}
                  {...register("gender")}
                />
                <img src={Other} alt="Outros" />
                <span>Outros</span>
              </label>
            </div>

            <h3>Teu Objetivo</h3>
            <div className="objectiveContainer">
              <label className="lose-weight">
                <input
                  type="radio"
                  value="Perder Peso"
                  className="radioInput"
                  onClick={() => setObjective("Perder Peso")}
                  {...register("objective")}
                />
                <img src={LoseWeight} alt="Perder Peso" />
                <span>Perder Peso</span>
              </label>
              <label className="gain-weight">
                <input
                  type="radio"
                  value="Ganhar Peso"
                  className="radioInput"
                  onClick={() => setObjective("Ganhar Peso")}
                  {...register("objective")}
                />
                <img src={GainWeight} alt="Ganhar Peso" />
                <span>Ganhar Peso</span>
              </label>
              <label className="gain-muscle">
                <input
                  type="radio"
                  value="Ganhar Músculos"
                  className="radioInput"
                  onClick={() => setObjective("Ganhar Músculos")}
                  {...register("objective")}
                />
                <img src={GainMuscle} alt="Ganhar músculos" />
                <span>Ter músculos</span>
              </label>
            </div>
          </div>
        ) : null}
        {step === 3 ? (
          <div className="step3">
            <h3>Tua atividade normal</h3>
            <div className="activityContainer">
              <label className="sedentary">
                <input
                  type="radio"
                  value="Sedentarismo"
                  className="radioInput"
                  onClick={() => setActivity("Sedentarismo")}
                  {...register("activity")}
                />
                <img src={Sedentarism} alt="Sedentarismo" />
                <span>Sedentarismo</span>
              </label>
              <div className="bottom">
                <label className="moderated">
                  <input
                    type="radio"
                    value="Moderado"
                    className="radioInput"
                    onClick={() => setActivity("Moderado")}
                    {...register("activity")}
                  />
                  <img src={Moderate} alt="Moderado" />
                  <span>Moderado</span>
                </label>
                <label className="intense">
                  <input
                    type="radio"
                    value="Intenso"
                    className="radioInput"
                    onClick={() => setActivity("Intenso")}
                    {...register("activity")}
                  />
                  <img src={Intense} alt="Intenso" />
                  <span>Intenso</span>
                </label>
              </div>
            </div>
          </div>
        ) : null}
        <>
          {step === 4 ? (
            <div className="step4">
              <ContainerRegister>
                <TextField
                  icon={AiFillMail}
                  label="Nome de Usuário"
                  name="name"
                  register={register}
                  placeholder="Ex.: romulociro"
                  error={errors.name?.message}
                />

                <TextField
                  icon={AiFillMail}
                  label="E-Mail"
                  name="email"
                  register={register}
                  placeholder="Ex.: romulo@mail.com"
                  error={errors.email?.message}
                />

                <TextField
                  icon={AiFillLock}
                  type="password"
                  label="Senha"
                  name="password"
                  register={register}
                  placeholder="Ex.: 123456"
                  error={errors.password?.message}
                />
              </ContainerRegister>
              <div className="button-page4">
                <Button
                  step={step}
                  backgroundColor={backgroundColor}
                  type="submit"
                  onClick={() => {
                    step > 1 ? setStep(step - 1) : setStep(1);
                  }}
                >
                  Anterior
                </Button>
                <Button
                  type="submit"
                  step={step}
                  backgroundColor={backgroundColor}
                >
                  Cadastrar
                </Button>
              </div>
              <p className="desc--Desktop__login">
                Já possui uma conta? <Link to="/login">Log In</Link>.
              </p>
            </div>
          ) : null}
        </>
      </form>

      {step !== 4 && (
        <div className="buttons">
          {step === 1 ? (
            <Button
              step={step}
              backgroundColor={backgroundColor}
              type="submit"
              onClick={() => {
                history.push("/");
              }}
            >
              Anterior
            </Button>
          ) : (
            <Button
              step={step}
              backgroundColor={backgroundColor}
              type="submit"
              onClick={() => {
                step > 1 ? setStep(step - 1) : setStep(1);
              }}
            >
              Anterior
            </Button>
          )}
          <Button
            step={step}
            backgroundColor={backgroundColor}
            type="submit"
            onClick={() => handleNextStep()}
          >
            Próximo
          </Button>
        </div>
      )}
    </Container>
  );
};
