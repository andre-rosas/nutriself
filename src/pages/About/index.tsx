import { Back } from "../../components/Back";
import { Container, ContainerCards } from "./styles";
import logo from "../../assets/logo-secondarySelf.png";
import vicent from "../../assets/vicente.png";
import andre from "../../assets/andre.png";
import igor from "../../assets/igor.jpg";
import lucas from "../../assets/lucas.png";
import osmar from "../../assets/osmar.png";
import romulo from "../../assets/romulo.png";
import GitLabIcon from "../../assets/gitlabIcon.png";
import LinkedInIcon from "../../assets/linkedInIcon.png"

import Button from "../../components/Button";
import { useHistory } from "react-router";

export const About = () => {
  const history = useHistory();

  return (
    <>
      <Back irPara="/profile" NomePage="Sobre" />
      <Container>
        <div className="title--container">
          <img className="logo--about" src={logo} alt="logo"></img>
          <div className="texto">
            <p>
              propõe a criação de uma aplicação em que as pessoas possam consultar
              valores nutricionais dos alimentos e montar um plano alimentar.
              Clientes podem se vincular a um nutricionista e estes são capazes de
              montar planos personalizados e adequados para os clientes que são
              capazes de fazer seus respectivos acompanhamentos.
            </p>
          </div>      
        </div>
          <div>
            <h2>A equipe:</h2>
          </div>
        <ContainerCards>

        <div className="card">
            <img src={osmar} alt="osmar"></img>
            <div className="user">
              <h4>Product Owner, PO</h4>
              <h3>Osmar Nothaft</h3>
              <div className="social">
                <div>
                  <p>GitLab</p>
                  <a href="https://gitlab.com/osmarn">
                  <img src={GitLabIcon} alt="gitlab" className="gitlab--about" />
                  </a>
                </div>
                <div>
                  <p>LinkedIn</p>
                  <a href="https://www.linkedin.com/in/osmarn/">
                  <img src={LinkedInIcon} alt="linkedin" className="linkedin--about" />
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div className="card">
            <img src={andre} alt="andre"></img>
            <div className="user">
              <h4>Tech Leader, TL</h4>
              <h3>André Rosas</h3>
              <div className="social">
                <div>
                  <p>GitLab</p>
                  <a href="https://gitlab.com/andre-rosas">
                  <img src={GitLabIcon} alt="gitlab" className="gitlab--about" />
                  </a>
                </div>
                <div>
                  <p>LinkedIn</p>
                  <a href="https://www.linkedin.com/in/andr%C3%A9-rosas-dev/">
                  <img src={LinkedInIcon} alt="linkedin" className="linkedin--about" />
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div className="card">
            <img src={lucas} alt="lucas"></img>
            <div className="user">
              <h4>Scrum Master, SM</h4>
              <h3>Lucas Ribeiro</h3>
              <div className="social">
                <div>
                  <p>GitLab</p>
                  <a href="https://gitlab.com/gutsberserk">
                  <img src={GitLabIcon} alt="gitlab" className="gitlab--about" />
                  </a>
                </div>
                <div>
                  <p>LinkedIn</p>
                  <a href="https://www.linkedin.com/in/lucas-ribeiroDev/">
                  <img src={LinkedInIcon} alt="linkedin" className="linkedin--about" />
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div className="card">
            <img src={igor} alt="igor"></img>
            <div className="user">
              <h4>Quality Assurance, QA</h4>
              <h3>Igor Petersson</h3>
              <div className="social">
                <div>
                  <p>GitLab</p>
                  <a href="https://gitlab.com/IgorPetersson">
                    <img src={GitLabIcon} alt="gitlab" className="gitlab--about" />
                  </a>
                </div>
                <div>
                  <p>LinkedIn</p>
                  <a href="https://www.linkedin.com/in/igor-petersson/">
                    <img src={LinkedInIcon} alt="linkedin" className="linkedin--about" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        
          <div className="card">
            <img src={romulo} alt="romulo"></img>
            <div className="user">
              <h4>Quality Assurance, QA</h4>
              <h3>Rômulo Ciro</h3>
              <div className="social">
                <div>
                  <p>GitLab</p>
                  <a href="https://gitlab.com/romulociro">
                  <img src={GitLabIcon} alt="gitlab" className="gitlab--about" />
                  </a>
                </div>
                <div>
                  <p>LinkedIn</p>
                  <a href="https://www.linkedin.com/in/romulociro">
                  <img src={LinkedInIcon} alt="linkedin" className="linkedin--about" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </ContainerCards>
        <Button
          type="submit"
          style={{ marginBottom: 36 }}
          onClick={() => history.push("/profile")}
        >
          Voltar
        </Button>
      </Container>
    </>
  );
};
