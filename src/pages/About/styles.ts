import styled from "styled-components";

export const Container = styled.div`
  flex-direction: column;
  align-items: center;
  display: flex;
  margin-top: 48px;
  @media (min-width: 768px){
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
  }
  div.title--container{
    display: flex;
    justify-content: center;
    flex-flow: row wrap;
    @media (min-width: 768px){
      display: flex;
      flex-flow: row wrap;
    }
  }

  img.logo--about {
      width: 280px;
      padding: 0 16px;
    @media (min-width: 768px){
      width: 400px;
      padding: 0 24px;
    }
  }
  .texto {
    font-family: 'Signika', sans-serif;
    font-size: 18px;
    font-style: normal;
    font-weight: 300;
    line-height: 24px;
    letter-spacing: -0.23999999463558197px;
    text-align: justify;
    color: var(--color-title);
    margin: 24px 48px 48px 48px;
    @media (min-width: 768px) {
      margin: 24px 48px 24px 48px
    }
  }
  .card {
    margin-top: 24px;
    margin-left: 8px;
    margin-right: 8px; 
    border-radius: 8px;
    display: flex;
    background-color: var(--color-primary);
    width: 280px;

    img {
      margin-top: 16px;
      width: 120px;
      height: 120px;
      border-radius: 8px;
    }
    .user {
      display: flex;
      flex-direction: column;
      align-content: space-between;
      justify-content: space-between;
      align-items: center;
      width: 180px;
      margin: 8px 0;
      h4 {
        margin: 0;
        color: var(--color-title);
        text-align: center;
        font-family: 'Roboto Condensed', sans-serif;
        font-size: 16px;
        padding-top: 2px;
      }
      h3 {
        color: var(--color-terciary);
        margin: 0;
        margin-top: 6px;
        margin-bottom: 8px;
        margin-left: 2px;
        margin-right: 2px;
        text-align: center;
        font-family: 'Roboto Condensed', sans-serif;
        font-size: 24px;
      }
      .social {
        display: flex;
        justify-content: space-between;
        svg {
          width: 48px;
          height: 48px;
        }
        div {
          display: flex;
          flex-direction: column;
          align-items: center;

          p {
            margin: 0;
            color: var(--color-text);
          } 
          a {
            text-decoration: none;

          }
          a:visited {
            text-decoration:none;
            color: transparent;
          }

          img.gitlab--about {
              width: 44px;
              height: 44px;
              margin-top: 0;
              }
          img.linkedin--about {
              width: 44px;
              height: 44px;
              margin-top: 0;
          }
          width: 70px;
          height: 70px;
          background-color: var(--color-base-default);
          border: 2px solid var(--color-quarternary);
          border-radius: 8px;
          &:hover{
            background-color: var(--color-terciary-two)
          }
          &:active {
            background-color: var(--color-terciary);
            color: var(--color-base-default);
            border: 2px solid var(--color-base-default)
          }
        }
      }
    }
}
`;
export const ContainerCards = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-bottom: 48px;
`;
