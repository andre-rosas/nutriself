import styled from "styled-components";

export const ContainerNotFound = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: space-evenly;
	align-content: center;
	min-height: 100vh;
	margin: 0 16px;
	@media (min-width: 768px){

	}
	img.Logo--notfound {
	width: 220px;
	padding-top: 16px;
		@media (min-width: 480px){
			width: 320px;
		}
		@media (min-width: 768px){
		width: 400px;
		}
	}
	img {
		width: 300px;
		@media (min-width: 480px){
			width: 448px;
		}
		@media (min-width: 768px){
			width: 600px;
		}	
	}

        p {
		font-size: 16px;
		color: var(--color-text);
		font-family: 'Signika', sans-serif;
		text-align: center;
		@media (min-width: 480px){
			font-size: 20px;
		}
			@media (min-width: 768px){
			font-size: 24px;
		}
		a {
		font-size: 32px;
		color: var(--color-primary);
		font-family: 'Signika', sans-serif;
		text-decoration: none;
		@media (min-width: 480px){
			font-size: 36px;
		}
			@media (min-width: 768px){
			font-size: 40px;
		}
			&:hover {
				text-decoration: none;
				color: var(--color-terciary);
			}
		}
	}
`;
