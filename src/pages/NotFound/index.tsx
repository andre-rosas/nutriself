import pageNotFound from "../../assets/pageNotFound.png"
import logoSecondarySelf from "../../assets/logo-secondarySelf.png"
import { ContainerNotFound } from "./styles"
import { Link } from "react-router-dom"


export const NotFound = () => {
	return (
		<ContainerNotFound>
			<div>
				<img className="Logo--notfound" src={logoSecondarySelf} alt="Logo" />
			</div>
			<div>
				<img src={pageNotFound} alt="Página não encontrada" />
			</div>
			<div>	
				<p>Desculpa, essa página que você está procurando não pode ser encontrada!</p>
			</div>
			<div>
				<p> Você pode voltar para a página inicial clicando em: <Link to="/">aqui</Link>.</p>
			</div>
		</ContainerNotFound>
	)
}