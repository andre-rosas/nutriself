import { Container } from "./styles";
import { useHistory } from "react-router-dom";
import { BsFillPersonFill } from "react-icons/bs";
import { FaClipboardList } from "react-icons/fa";
import { AiFillExclamationCircle } from "react-icons/ai";
import { IoIosPaper } from "react-icons/io";
import { RiLogoutBoxRFill } from "react-icons/ri";
import { Back } from "../../components/Back";
import { Navbar } from "../../components/NavBar";
import { useEditProfileContext } from "../../providers/EditProfile";
import { useNavBar } from "../../providers/NavBar";
import { useEffect } from "react";
import { Header } from "../../components/Header";

export const Profile = () => {
  const { changeSelected } = useNavBar();
  const history = useHistory();
  const { dadosDoUsuario, PegarDadosDoUsuario, dados } =
    useEditProfileContext();
  useEffect(() => {
    PegarDadosDoUsuario();
  }, [dados]);
  const deslogar = () => {
    localStorage.clear();
    history.push("/");
  };
  return (
    <>
      <Header />
      <Back irPara="/dietPlan/choose" NomePage="Perfil" />
      <Container>
        <div className="profileImg">
          <p className="letter">{dados && dadosDoUsuario.name[0]}</p>
        </div>
        {dados && <h3>{dadosDoUsuario.name}</h3>}
        {dados && <h4>{dadosDoUsuario.type}</h4>}
        <div className="options" onClick={() => history.push("/editprofile")}>
          <div className="icone">
            <BsFillPersonFill />{" "}
          </div>
          Editar Perfil
          <span>&#62;</span>
        </div>
        <div
          className="options"
          onClick={() => {
            history.push("/dietplan/choose");
            changeSelected("home");
          }}
        >
          <div className="icone">
            <FaClipboardList />
          </div>
          Planos de Dieta
          <span>&#62;</span>
        </div>
        <div className="options" onClick={() => history.push("/about")}>
          <div className="icone">
            <AiFillExclamationCircle />
          </div>
          Sobre
          <span>&#62;</span>
        </div>
        <div className="options" onClick={() => history.push("/terms")}>
          <div className="icone">
            <IoIosPaper />
          </div>
          Termos e Políticas
          <span>&#62;</span>
        </div>
        <div className="options" onClick={() => deslogar()}>
          <div className="icone">
            <RiLogoutBoxRFill />
          </div>
          Log Out
          <span>&#62;</span>
        </div>
      </Container>
      <Navbar />
    </>
  );
};
