import logoSecondarySelf from "../../assets/logo-secondarySelf.png";
import { Link } from "react-router-dom";
import FormLogin from "../../components/FormLogin";
import { ContainerLogin, LoginImg, LoginForm, BackgroundL } from "./styles";
import { AnimationContainer } from "./styles";

export const Login = () => {
  return (
    <ContainerLogin>
      <AnimationContainer>
        <LoginForm>
          <LoginImg>
            <img
              className="Logo--Desktop__login"
              src={logoSecondarySelf}
              alt="Logo"
            />
          </LoginImg>
          <h2>Login</h2>
          <FormLogin />
          <p className="desc--Desktop__login">
            Não possui uma conta? Registre-se <Link to="/register">aqui</Link>.
          </p>
        </LoginForm>
      </AnimationContainer>
      <BackgroundL />
    </ContainerLogin>
  );
};
