import styled, { keyframes } from "styled-components";
import BackgroundLogin from "../../assets/BackgroundLogin.png";

export const ContainerLogin = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  height: 100vh;
  width: 100%;

  p.desc--Desktop__login {
    font-size: 16px;
    color: var(--color-text);
    font-family: "Signika", sans-serif;
    text-align: center;
    @media (min-width: 480px) {
      font-size: 24px;
    }
    a {
      font-size: 32px;
      color: var(--color-primary);
      font-family: "Signika", sans-serif;
      text-decoration: none;
      @media (min-width: 768px) {
      }
      &:hover {
        text-decoration: none;
        color: var(--color-terciary);
      }
    }
  }
`;

export const BackgroundL = styled.div`
  display: none;
  @media (min-width: 900px) {
    display: block;
    flex: 1;
    background: url(${BackgroundLogin}) no-repeat center, var(--color-primary);
    background-color: var(--color-primary);
    width: 700px;
  }
`;

export const LoginImg = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  @media (min-width: 768px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 0 16px;
  }
  img {
    width: 220px;
  }
`;

export const LoginForm = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  align-content: center;
  justify-content: space-evenly;
  height: 100vh;
  margin: 0 16px;

  @media (min-width: 768px) {
  }
  form {
    text-align: center;
    height: 320px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-evenly;
  }
`;

const appearFromRight = keyframes`
	from {
		opacity: 0;
		transform: translateX(50px)
	}
	to {
		opacity: 1;
		transform: translateX(0px)
	}
`;

export const AnimationContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  animation: ${appearFromRight} 1s;
`;
