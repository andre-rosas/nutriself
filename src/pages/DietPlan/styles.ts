import styled from "styled-components";

export const AllContainer = styled.div`
  @media (min-width: 768px) {
    .navBar {
      display: none;
    }
  }
`;

export const Container = styled.div`
  height: calc(100vh - 180px);

  .miniProgressBar__mobile {
    display: flex;
    flex-direction: row;
  }
  .miniDesktop {
    display: none;
  }
  .moreDetailDesktop {
    display: none;
  }
  .more__list {
    display: flex;
    flex-direction: column;
  }

  p.warning--nutri {
    color: var(--color-terciary);
    margin-top: 8px;
    font-size: 16px;
    @media (min-width: 768px) {
      font-size: 20px;
    }
    @media (min-width: 1100px) {
      font-size: 24px;
    }
  }

  p.plan--card__nutri {
    color: var(--color-placeholder);
  }

  @media (min-width: 768px) {
    height: calc(100vh - 130px);
    min-height: 0;
    margin: 0 auto;
    .miniProgressBar__mobile {
      display: none;
    }
    .contentContainer {
    }
    .miniDesktop {
      display: block;
    }
    .more__list {
      flex-direction: row;
      height: 500px;
      justify-content: center;
      padding-top: 20px;
    }
    .moreDetailDesktop {
      display: block;
      width: 50vh;
    }
    .moreDetail {
      display: none;
    }
    .circular__mini {
      display: flex;
      align-items: center;
      justify-content: center;
      align-content: center;
      width: 740px;
      margin: 0 auto;
      height: 200px;
      border-bottom: 1px solid black;
      padding-bottom: 30px;
      margin-bottom: 10px;
    }
    .list {
      width: 50vh;
      overflow: auto;
      height: 500px;
    }
    .list::-webkit-scrollbar {
      display: none;
    }
  }
`;
export const Choise = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: fit-content;

  .available {
    cursor: pointer;
    transition: all 0.2s;
  }

  .available:hover {
    transform: translateY(-2px);
    box-shadow: 0px 2px 3px grey;
  }

  .escolha {
    display: flex;
    justify-content: space-around;
    background-color: #eff7ee;
    border-radius: 8px;
    width: 250px;
    height: 100px;
    margin-top: 50px;

    img {
      width: 80px;
      height: 80px;
      margin-top: 10px;
    }

    p {
      margin-top: 15px;
      color: #2e2e2e;
      font-family: "Signika", sans-serif;
      font-size: 20px;
      font-style: normal;
      font-weight: 400;
      line-height: 22px;
      letter-spacing: -0.23999999463558197px;
      text-align: left;
    }
  }
`;
